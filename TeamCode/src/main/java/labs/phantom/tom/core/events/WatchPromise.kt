package labs.phantom.tom.core.events

class WatchPromise<T: GameEvent>(public val emitter: GameEventEmitter<T>) { }