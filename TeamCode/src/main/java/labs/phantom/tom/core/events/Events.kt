package labs.phantom.tom.core.events

typealias GameEventCallback<T> = (e: T) -> Unit;

infix fun <T: GameEvent> Watch.by(eventEmitter: GameEventEmitter<T>): WatchPromise<T> {
    return WatchPromise<T>(eventEmitter);
}

infix fun <T: GameEvent> WatchPromise<T>.using(that: GameEventCallback<T>) {
    this.emitter.register(that);
}