package labs.phantom.tom.core.logic.module

enum class GameModuleTriggers {
    CONFIGURE,
    INIT,
    START,
    UPDATE,
    STOP
}