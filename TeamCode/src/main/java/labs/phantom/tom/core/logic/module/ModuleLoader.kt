package labs.phantom.tom.core.logic.module

import labs.phantom.tom.core.logic.GameUnit

class ModuleLoader(public val unit: GameUnit) { }

infix fun ModuleLoader.load(that: GameModule) {
    this.unit.addModule(that);
}