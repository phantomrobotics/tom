package labs.phantom.tom.core.tooling

import labs.phantom.tom.core.logic.GameUnit

typealias VoidCallback = () -> Unit;
typealias ConfigureCallback = (u: GameUnit) -> Unit;
typealias ParameterizedCallback<T> = (u: T) -> Unit;