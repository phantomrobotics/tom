package labs.phantom.tom.core.hardware.gamepad

enum class Gamepads {
    FIRST,
    SECOND
}