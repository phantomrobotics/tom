package labs.phantom.tom.core.hardware.gamepad

import com.qualcomm.robotcore.hardware.Gamepad
import labs.phantom.tom.core.events.GameEvent

class GamepadEvent(public val pad: Gamepad, public val source: Gamepads): GameEvent() {

}