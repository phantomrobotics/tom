package labs.phantom.tom.core.logic.module

import com.qualcomm.robotcore.hardware.HardwareMap
import labs.phantom.tom.core.events.Watch
import labs.phantom.tom.core.logic.GameUnit
import labs.phantom.tom.core.logic.assembly.AssemblyLoader
import labs.phantom.tom.core.logic.assembly.GameAssembly
import labs.phantom.tom.core.tooling.ParameterizedCallback
import labs.phantom.tom.core.tooling.VoidCallback

abstract class GameModule(public val updateable: Boolean = false) {
    private var configureCallback: ParameterizedCallback<GameModule> = { };
    private var initializedCallback: VoidCallback = { };
    private var startedCallback: VoidCallback = { };
    private var updatedCallback: VoidCallback = { };
    private var stoppedCallback: VoidCallback = { };

    public fun configure(configureCallback: ParameterizedCallback<GameModule>) {
        this.configureCallback = configureCallback;
    }

    public fun initialized(initializedCallback: VoidCallback) {
        this.initializedCallback = initializedCallback;
    }

    public fun started(startedCallback: VoidCallback) {
        this.startedCallback = startedCallback;
    }

    public fun updated(updatedCallback: VoidCallback) {
        this.updatedCallback = updatedCallback;
    }

    public fun stopped(stoppedCallback: VoidCallback) {
        this.stoppedCallback = stoppedCallback;
    }

    private val assemblyList: MutableList<GameAssembly> = mutableListOf();
    public fun <Z: GameAssembly> addAssembly(assembly: Z) {
        this.assemblyList.add(assembly);
    }
    public var assemblies: AssemblyLoader;

    public lateinit var unit: GameUnit;
    public lateinit var watch: Watch;
    public lateinit var hardware: HardwareMap;

    init {
        this.assemblies = AssemblyLoader(this);
    }

    public fun install(unit: GameUnit, watch: Watch, hardware: HardwareMap) {
        this.unit = unit;
        this.watch = watch;
        this.hardware = hardware;

        for(assembly in assemblyList) assembly.install(unit, this, watch, hardware);
    }

    public fun launch(what: GameModuleTriggers) {
        for(assembly in assemblyList) assembly.launch(what);

        when(what) {
            GameModuleTriggers.CONFIGURE -> configureCallback(this)
            GameModuleTriggers.INIT -> initializedCallback()
            GameModuleTriggers.START -> startedCallback()
            GameModuleTriggers.UPDATE -> { if(this.updateable) updatedCallback(); }
            GameModuleTriggers.STOP -> stoppedCallback()
        }
    }
}