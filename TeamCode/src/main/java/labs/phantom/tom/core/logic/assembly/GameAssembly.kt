package labs.phantom.tom.core.logic.assembly

import com.qualcomm.robotcore.hardware.HardwareMap
import labs.phantom.tom.core.events.Watch
import labs.phantom.tom.core.logic.GameUnit
import labs.phantom.tom.core.logic.module.GameModule
import labs.phantom.tom.core.logic.module.GameModuleTriggers
import labs.phantom.tom.core.tooling.ParameterizedCallback
import labs.phantom.tom.core.tooling.VoidCallback

abstract class GameAssembly(public val updateable: Boolean) {
    private var configureCallback: ParameterizedCallback<GameAssembly> = {};
    private var initializedCallback: VoidCallback = {};
    private var startedCallback: VoidCallback = { };
    private var updatedCallback: VoidCallback = { };
    private var stoppedCallback: VoidCallback = { };

    public fun configure(configureCallback: ParameterizedCallback<GameAssembly>) {
        this.configureCallback = configureCallback;
    }

    public fun initialized(initializedCallback: VoidCallback) {
        this.initializedCallback = initializedCallback;
    }

    public fun started(startedCallback: VoidCallback) {
        this.startedCallback = startedCallback;
    }

    public fun updated(updatedCallback: VoidCallback) {
        this.updatedCallback = updatedCallback;
    }

    public fun stopped(stoppedCallback: VoidCallback) {
        this.stoppedCallback = stoppedCallback;
    }

    public lateinit var unit: GameUnit;
    public lateinit var module: GameModule;
    public lateinit var watch: Watch;
    public lateinit var hardware: HardwareMap;

    public fun install(unit: GameUnit, module: GameModule, watch: Watch, hardware: HardwareMap) {
        this.unit = unit;
        this.module = module;
        this.watch = watch;
        this.hardware = hardware;
    }

    public fun launch(what: GameModuleTriggers) {
        when(what) {
            GameModuleTriggers.CONFIGURE -> configureCallback(this)
            GameModuleTriggers.INIT -> initializedCallback()
            GameModuleTriggers.START -> startedCallback()
            GameModuleTriggers.UPDATE -> { if(this.updateable) updatedCallback(); }
            GameModuleTriggers.STOP -> stoppedCallback()
        }
    }
}