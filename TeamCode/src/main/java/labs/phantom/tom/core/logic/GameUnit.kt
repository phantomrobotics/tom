package labs.phantom.tom.core.logic

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.hardware.Gamepad
import labs.phantom.tom.core.events.Watch
import labs.phantom.tom.core.logic.module.GameModule
import labs.phantom.tom.core.logic.module.GameModuleTriggers
import labs.phantom.tom.core.logic.module.ModuleLoader
import labs.phantom.tom.core.tooling.ConfigureCallback
import labs.phantom.tom.core.tooling.VoidCallback

abstract class GameUnit(private val updateable: Boolean = false): LinearOpMode() {
    private var configurareCallback: ConfigureCallback = {};

    private var initializedCallback: VoidCallback = {};
    private var startedCallback: VoidCallback = {};
    private var updatedCallback: VoidCallback = {};
    private var stoppedCallback: VoidCallback = {};

    var hardware = hardwareMap;

    public fun configure(callback: ConfigureCallback) {
        this.configurareCallback = callback;
    }

    public fun initialized(callback: VoidCallback) {
        this.initializedCallback = callback;
    }

    public fun started(callback: VoidCallback) {
        this.startedCallback = callback;
    }

    public fun updated(callback: VoidCallback) {
        this.updatedCallback = callback;
    }

    public fun stopped(callback: VoidCallback) {
        this.stoppedCallback = callback;
    }

    public val moduleList: MutableList<GameModule> = mutableListOf();
    public fun addModule(module: GameModule) {
        this.moduleList.add(module);
    }
    public var modules: ModuleLoader;
    init {
        this.modules = ModuleLoader(this);
    }

    public val watch = Watch();

    override fun runOpMode() {
        this.hardware = hardwareMap;
        for(module in moduleList) module.install(this, watch, hardware);

        for(module in moduleList) module.launch(GameModuleTriggers.CONFIGURE);
        this.configurareCallback(this);

        for(module in moduleList) module.launch(GameModuleTriggers.INIT);
        this.initializedCallback();

        this.waitForStart();

        for(module in moduleList) module.launch(GameModuleTriggers.START);
        this.startedCallback();

        while(this.opModeIsActive() && this.updateable) {
            for(module in moduleList) module.launch(GameModuleTriggers.UPDATE);
            this.updatedCallback();
        }

        for(module in moduleList) module.launch(GameModuleTriggers.STOP);
        this.stoppedCallback();
    }
}