package labs.phantom.tom.core.hardware.gamepad

infix fun Gamepads.sourced(that: GamepadEvent): Boolean {
    return that.source == this;
}

object GAMEPAD {
    public val events = GamepadEventEmitter();
}