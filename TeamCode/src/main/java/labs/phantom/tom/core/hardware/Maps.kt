package labs.phantom.tom.core.hardware

import com.qualcomm.robotcore.hardware.HardwareDevice
import com.qualcomm.robotcore.hardware.HardwareMap

infix fun <T: HardwareDevice> HardwareMap.export(that: String): T {
    @Suppress("UNCHECKED_CAST")
    return this.get(that) as T;
}