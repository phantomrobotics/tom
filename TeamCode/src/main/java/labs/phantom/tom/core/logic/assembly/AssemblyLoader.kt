package labs.phantom.tom.core.logic.assembly

import labs.phantom.tom.core.logic.module.GameModule

class AssemblyLoader(public val module: GameModule) { }

infix fun AssemblyLoader.load(that: GameAssembly) {
    this.module.addAssembly(that);
}