package labs.phantom.tom.core.hardware.gamepad

import com.qualcomm.robotcore.hardware.Gamepad
import labs.phantom.tom.core.logic.GameUnit
import labs.phantom.tom.core.logic.module.GameModule

class GamepadModule: GameModule(updateable = true) {

    private val states: MutableMap<Gamepads, ByteArray> = mutableMapOf(
            Gamepads.FIRST to ByteArray(100),
            Gamepads.SECOND to ByteArray(100)
    );

    private fun detectChanges(g: Gamepad, source: Gamepads) {
        val bytes = g.toByteArray();
        for(index in bytes.indices) {
            if(bytes[index] != this.states[source]!![index]) {
                GAMEPAD.events.trigger(GamepadEvent(g, source));
            }
        }

    }

    init {
        this.configure {
            it.updated {
                this.detectChanges(it.unit.gamepad1, Gamepads.FIRST);
                this.detectChanges(it.unit.gamepad2, Gamepads.SECOND);
            }

            it.stopped {
                GAMEPAD.events.clear();
            }
        }
    }
}