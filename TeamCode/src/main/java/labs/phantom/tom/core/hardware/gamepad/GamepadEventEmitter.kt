package labs.phantom.tom.core.hardware.gamepad

import labs.phantom.tom.core.events.GameEventEmitter

class GamepadEventEmitter: GameEventEmitter<GamepadEvent>() {}