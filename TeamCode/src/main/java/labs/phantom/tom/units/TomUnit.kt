package labs.phantom.tom.units

import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import labs.phantom.tom.core.hardware.gamepad.GamepadModule
import labs.phantom.tom.core.logic.GameUnit
import labs.phantom.tom.core.logic.module.load
import labs.phantom.tom.modules.actionable.CollectorModule
import labs.phantom.tom.modules.actionable.DriveModule
import labs.phantom.tom.modules.thrower.ThrowerModule
import labs.phantom.tom.modules.gearbox.GearboxModule

@TeleOp(name = "Tom")
class TomUnit: GameUnit(updateable = true) {
    init {
        modules load GamepadModule();

        modules load GearboxModule();
        modules load DriveModule();

        modules load CollectorModule();
        modules load ThrowerModule();
    }
}