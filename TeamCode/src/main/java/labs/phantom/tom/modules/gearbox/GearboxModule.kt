package labs.phantom.tom.modules.gearbox

import labs.phantom.tom.core.events.by
import labs.phantom.tom.core.events.using
import labs.phantom.tom.core.hardware.gamepad.GAMEPAD
import labs.phantom.tom.core.hardware.gamepad.Gamepads
import labs.phantom.tom.core.hardware.gamepad.sourced
import labs.phantom.tom.core.logic.module.GameModule
import labs.phantom.tom.events.Events

class GearboxModule: GameModule(updateable = false) {
    init {
        this.configure {
            watch by GAMEPAD.events using {
                if(Gamepads.FIRST sourced it) {
                    if(it.pad.x) {
                        Events.gearboxChange.trigger(GearboxChangeEvent(GearboxChangeDirection.DOWN));
                        unit.sleep(50);
                    } else if(it.pad.b) {
                        Events.gearboxChange.trigger(GearboxChangeEvent(GearboxChangeDirection.UP));
                        unit.sleep(50);
                    }
                }
            }

            it.stopped {
                Events.gearboxChange.clear();
            }
        }
    }
}