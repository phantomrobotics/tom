package labs.phantom.tom.modules.thrower

import labs.phantom.tom.core.events.GameEvent

class ThrowerStateChangeEvent(public val inAir: Boolean): GameEvent() { }