package labs.phantom.tom.modules.actionable

import labs.phantom.tom.assemblies.CollectorAssembly
import labs.phantom.tom.core.events.by
import labs.phantom.tom.core.events.using
import labs.phantom.tom.core.hardware.gamepad.GAMEPAD
import labs.phantom.tom.core.hardware.gamepad.Gamepads
import labs.phantom.tom.core.hardware.gamepad.sourced
import labs.phantom.tom.core.logic.assembly.load
import labs.phantom.tom.core.logic.module.GameModule
import labs.phantom.tom.events.Events

class CollectorModule: GameModule(updateable = true) {
    init {
        val collector = CollectorAssembly()

        assemblies load collector;

        this.configure {
            watch by Events.throwerChange using  {
                collector.lock(it.inAir);

                if(!it.inAir) {
                    collector.set(unit.gamepad2.left_stick_y, unit.gamepad2.right_stick_y);
                }
            }

            watch by GAMEPAD.events using {
                if(Gamepads.SECOND sourced it) {
                    collector.set(it.pad.left_stick_y, it.pad.right_stick_y);
                }
            }
        }
    }
}