package labs.phantom.tom.modules.thrower

import labs.phantom.tom.core.events.GameEventEmitter

class ThrowerStateChangeEventEmitter: GameEventEmitter<ThrowerStateChangeEvent>() {}