package labs.phantom.tom.modules.gearbox

enum class GearboxChangeDirection {
    UP,
    DOWN
}