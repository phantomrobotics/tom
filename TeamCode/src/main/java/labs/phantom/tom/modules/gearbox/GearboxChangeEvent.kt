package labs.phantom.tom.modules.gearbox

import labs.phantom.tom.core.events.GameEvent

class GearboxChangeEvent(public val direction: GearboxChangeDirection): GameEvent() { }