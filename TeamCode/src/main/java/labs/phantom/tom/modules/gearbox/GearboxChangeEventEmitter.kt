package labs.phantom.tom.modules.gearbox

import labs.phantom.tom.core.events.GameEventEmitter

class GearboxChangeEventEmitter: GameEventEmitter<GearboxChangeEvent>() { }