package labs.phantom.tom.modules.actionable

import labs.phantom.tom.assemblies.DriveAssembly
import labs.phantom.tom.core.events.by
import labs.phantom.tom.core.events.using
import labs.phantom.tom.core.hardware.gamepad.GAMEPAD
import labs.phantom.tom.core.hardware.gamepad.Gamepads
import labs.phantom.tom.core.hardware.gamepad.sourced
import labs.phantom.tom.core.logic.assembly.load
import labs.phantom.tom.core.logic.module.GameModule
import labs.phantom.tom.events.Events

class DriveModule: GameModule(updateable = true) {
    init {
        val drive = DriveAssembly();

        assemblies load drive;

        this.configure {
            watch by GAMEPAD.events using {
                if(Gamepads.FIRST sourced it) drive.set(-it.pad.left_stick_x, it.pad.left_stick_y, -it.pad.right_stick_x);
            }

            watch by Events.gearboxChange using  {
                drive.changeGear(it.direction);
            }
        }
    }
}