package labs.phantom.tom.modules.thrower

import labs.phantom.tom.assemblies.ThrowerAssembly
import labs.phantom.tom.core.events.by
import labs.phantom.tom.core.events.using
import labs.phantom.tom.core.hardware.gamepad.GAMEPAD
import labs.phantom.tom.core.hardware.gamepad.Gamepads
import labs.phantom.tom.core.hardware.gamepad.sourced
import labs.phantom.tom.core.logic.assembly.load
import labs.phantom.tom.core.logic.module.GameModule

class ThrowerModule: GameModule(updateable = true) {
    init {
        val thrower = ThrowerAssembly()

        assemblies load thrower;

        this.configure {
            watch by GAMEPAD.events using {
                if(Gamepads.FIRST sourced it) {
                    if(it.pad.a) thrower.toggle();
                }
            }
        }
    }
}