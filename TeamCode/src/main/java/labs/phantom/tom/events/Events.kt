package labs.phantom.tom.events

import labs.phantom.tom.modules.gearbox.GearboxChangeEventEmitter
import labs.phantom.tom.modules.thrower.ThrowerStateChangeEventEmitter

object Events {
    public val gearboxChange = GearboxChangeEventEmitter();
    public val throwerChange = ThrowerStateChangeEventEmitter()
}