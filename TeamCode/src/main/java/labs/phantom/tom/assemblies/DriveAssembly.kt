package labs.phantom.tom.assemblies

import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import labs.phantom.tom.core.hardware.export
import labs.phantom.tom.core.logic.assembly.GameAssembly
import labs.phantom.tom.modules.gearbox.GearboxChangeDirection

class DriveAssembly: GameAssembly(updateable = true) {
    lateinit var leftFrontMotor: DcMotor;
    lateinit var rightFrontMotor: DcMotor;
    lateinit var leftBackMotor: DcMotor;
    lateinit var rightBackMotor: DcMotor;

    private val gearPowers = listOf(0.25, 0.5, 0.75, 1.0);
    private var power = 0.75;
    private var gear = 2
    set(value) {
        if(value >= 0 && value < gearPowers.count()) {
            power = gearPowers[value];
            field = value;
        }
    }

    init {
        this.configure {
            leftFrontMotor = hardware export "left_front";
            rightFrontMotor = hardware export "right_front";
            leftBackMotor = hardware export "left_back";
            rightBackMotor = hardware export "right_back";

            it.initialized {
                leftFrontMotor.direction = DcMotorSimple.Direction.REVERSE;
                leftBackMotor.direction = DcMotorSimple.Direction.REVERSE;

                this.stop();
            }

            it.stopped {
                this.stop()
            }
        }
    }

    public fun changeGear(direction: GearboxChangeDirection) {
        if(direction == GearboxChangeDirection.UP) this.gear++;
        if(direction == GearboxChangeDirection.DOWN) this.gear--;
    }

    public fun stop() {
        leftFrontMotor.power = 0.0;
        rightFrontMotor.power = 0.0;
        leftBackMotor.power = 0.0;
        rightBackMotor.power = 0.0;
    }

    public fun set(x: Float, y: Float, rotation: Float) {
        val r = Math.hypot(x.toDouble(), y.toDouble());
        val robotAngle = Math.atan2(y.toDouble(), x.toDouble()) - Math.PI / 4;

        leftFrontMotor.power = power * (r * Math.cos(robotAngle) + rotation.toDouble());
        rightFrontMotor.power = power * (r * Math.sin(robotAngle) - rotation.toDouble());
        leftBackMotor.power = power * (r * Math.sin(robotAngle) + rotation.toDouble());
        rightBackMotor.power = power * (r * Math.cos(robotAngle) - rotation.toDouble());
    }
}