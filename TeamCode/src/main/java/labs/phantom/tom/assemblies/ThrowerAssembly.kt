package labs.phantom.tom.assemblies

import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import com.qualcomm.robotcore.hardware.TouchSensor
import labs.phantom.tom.core.hardware.export
import labs.phantom.tom.core.logic.assembly.GameAssembly
import labs.phantom.tom.events.Events
import labs.phantom.tom.modules.thrower.ThrowerStateChangeEvent

class ThrowerAssembly: GameAssembly(updateable = true) {
    lateinit var throwerMotor: DcMotor;
    lateinit var startThrowerButton: TouchSensor;
    lateinit var endThrowerButton: TouchSensor;

    public val airTime: Long = 3000;
    private val throwSpeed: Double = 0.3;
    private val downSpeed: Double = 0.25;

    private var throwState: Boolean = false;
    private var liftState: Boolean = false;
    private var liftTime: Long = 0;

    init {
        this.configure {
            throwerMotor = hardware export "thrower";
            startThrowerButton = hardware export "start_thrower";
            endThrowerButton = hardware export "end_thrower";

            it.initialized {
                this.stop();
            }

            it.updated {
                if(liftState) {
                    if(System.currentTimeMillis() - this.liftTime > airTime) {
                        this.reset();
                        this.throwState = !this.throwState;
                    }

                    if(startThrowerButton.isPressed() && this.throwState == false) {
                        this.reset();
                        Events.throwerChange.trigger(ThrowerStateChangeEvent(inAir = false));
                    }

                    if(endThrowerButton.isPressed() && this.throwState == true) {
                        this.reset();
                    }
                }
            }

            it.stopped {
                this.stop();
            }
        }
    }

    private fun reset() {
        this.stop();
        this.liftTime = 0;
        this.liftState = false;
    }

    public fun stop() {
        throwerMotor.power = 0.0;
    }

    public fun toggle() {
        if(this.liftState) return;

        if(!this.throwState) {
            this.liftTime = System.currentTimeMillis();
            this.liftState = true;
            this.throwState = true;
            throwerMotor.power = throwSpeed;
            Events.throwerChange.trigger(ThrowerStateChangeEvent(inAir = true));
        } else {
            this.liftTime = System.currentTimeMillis();
            this.liftState = true;
            this.throwState = false;
            throwerMotor.power = -downSpeed;
        }
        unit.sleep(100);
    }
}