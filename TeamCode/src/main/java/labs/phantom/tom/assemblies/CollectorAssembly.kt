package labs.phantom.tom.assemblies

import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import labs.phantom.tom.core.hardware.export
import labs.phantom.tom.core.logic.assembly.GameAssembly

class CollectorAssembly: GameAssembly(updateable = false) {
    lateinit var leftMotor: DcMotor;
    lateinit var rightMotor: DcMotor;

    private var locked: Boolean = false;

    init {
        this.configure {
            leftMotor = hardware export "left_pull";
            rightMotor = hardware export "right_pull";

            it.initialized {
                rightMotor.direction = DcMotorSimple.Direction.REVERSE;

                this.stop();
            }

            it.stopped {
                this.stop();
            }
        }
    }

    public fun stop() {
        leftMotor.power = 0.0;
        rightMotor.power = 0.0;
    }

    public fun set(left: Float, right: Float) {
        if(this.locked) return;

        leftMotor.power = -left.toDouble();
        rightMotor.power = -right.toDouble();
    }

    public fun lock(state: Boolean) {
        this.locked = state;

        if(state) {
            this.stop();
        }
    }
}